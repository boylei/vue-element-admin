import request from '@/utils/request'
import axios from 'axios'

export function userlogin(data) {
  return request({
    url: '/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  // return request({

  // })

  return axios({
    url: '/dev-api/vue-element-admin/user/info',
    method: 'get',
    params: { token }
  })

}

export function logout() {
  return request({
    url: '/vue-element-admin/user/logout',
    method: 'post'
  })
}
