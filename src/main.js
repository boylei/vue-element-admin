import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'
import enLang from 'element-ui/lib/locale/lang/en'// 如果使用中文语言包请默认支持，无需额外引入，请删除该依赖

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  locale: enLang // 如果使用中文，无需设置，请删除
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

var routeform = false

// router.beforeEach((to, from, next) => {

//   console.log(from)
//   console.log(to)
//   console.log('---------------------------')

//   let storeinfo = JSON.parse(sessionStorage.getItem('store'))
//   console.log(storeinfo)

//   let userinfo = JSON.parse(sessionStorage.getItem('store'))
//   console.log(userinfo)
  
//   // let routeform = JSON.parse(localStorage.getItem('routeform'))
//   console.log(routeform)

//   if (to.fullPath == '/login') {
//     next()
//     console.log('tologin')
//   }
//   else if((storeinfo!=null)||(userinfo!=null)) {
//     if (routeform==false) { // 判断当前用户是否已拉取完user_info信息
//       // console.log(store.getters.roles)
//       // const roles = new Array
//       // roles[0] = 'store'
//       const roles = storeinfo==null?userinfo.roles:storeinfo.roles
//       store.dispatch('permission/generateRoutes', roles).then((res) => { // 生成可访问的路由表
//         console.log(res)
//         router.addRoutes(res) // 动态添加可访问路由表
//         console.log('add route')
//         routeform = true
//         // localStorage.setItem("routeform", JSON.stringify('true'))
//         next({ ...to, replace: true }) // hack方法 确保addRoutes已完成 ,set the replace: true so the navigation will not leave a history record
//       }).catch((err)=>{
//         console.log(err)
//         return
//       })
//     } else {
//       console.log('pass')
//       next() //当有用户权限的时候，说明所有可访问路由已生成 如访问没权限的全面会自动进入404页面

//     }
//   }
//   else {
//       // console.log(sessionStorage.getItem('store'))
//       console.log('redirect login')
//       next('/login')
//     }
// })

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
