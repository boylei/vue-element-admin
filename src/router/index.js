import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import componentsRouter from './modules/components'
import chartsRouter from './modules/charts'
import tableRouter from './modules/table'
import nestedRouter from './modules/nested'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    noCache: true                if set true, the page will no be cached(default is false)
    affix: true                  if set true, the tag will affix in the tags-view
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/auth-redirect'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: '/value/valueMainPage'
  },
  {
    path: '/index',
    component: Layout,
    redirect: '/value/valueMainPage'
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [

  /** when your routing map is too long, you can split it into small modules **/
  // componentsRouter,
  // chartsRouter,
  // nestedRouter,
  // tableRouter,
  {
    path: '/value',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'storeValue',
    meta: {
      title: '价值',
      icon: 'money',
      roles: ['store']
    },
    children: [
      // {
      //   path: 'getValueWeight',
      //   component: () => import('@/views/platform/value/getValueWeight'),
      //   name: 'valueRadar',
      //   meta: { title: '价值雷达图', icon: 'chart', roles: ['store'] }
      // },
      {
        path: 'valueMainPage',
        component: () => import('@/views/platform/value/valueMain'),
        name: 'valueMainPage',
        meta: { title: '欢迎界面', icon: 'star', roles: ['store'] }
      }
    ]
  },
  {
    path: '/tobuy',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'Table',
    meta: {
      title: '求购',
      icon: 'table',
      roles: ['store']
    },
    children: [
      {
        path: 'tobuy-table',
        component: () => import('@/views/platform/wantTobuy/tobuy'),
        name: 'BuyTable',
        meta: { title: '求购平台', roles: ['store'] }
      },
      {
        path: 'my-tobuy-table',
        component: () => import('@/views/platform/wantTobuy/my-tobuy'),
        name: 'MyBuyTable',
        meta: { title: '本店求购', roles: ['store'] }
      }
    ]
  },
  {
    path: '/tosell',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'Table',
    meta: {
      title: '求售',
      icon: 'table',
      roles: ['store']
    },
    children: [
      {
        path: 'tosell-table',
        component: () => import('@/views/platform/wantTosell/tosell'),
        name: 'SellTable',
        meta: { title: '求售平台', roles: ['store'] }
      },
      {
        path: 'my-tosell-table',
        component: () => import('@/views/platform/wantTosell/my-tosell'),
        name: 'MySellTable',
        meta: { title: '本店求售', roles: ['store'] }
      }
    ]
  },
  {
    path: '/quotation',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'Quotation',
    meta: {
      title: '报价单',
      icon: 'table',
      roles: ['store']
    },
    children: [
      {
        path: 'buy',
        component: () => import('@/views/platform/quo/buy'),
        name: 'valueRadar',
        meta: { title: '购买报价', icon: 'chart', roles: ['store'] }
      },
      {
        path: 'sell',
        component: () => import('@/views/platform/quo/sell'),
        name: 'valueRadar',
        meta: { title: '出售报价', icon: 'chart', roles: ['store'] }
      },
    ]
  },
  {
    path: '/fight',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'Fight',
    meta: {
      title: '拼单',
      icon: 'table',
      roles: ['store']
    },
    children: [
      {
        path: 'fightOrder',
        component: () => import('@/views/platform/pindan/store-pindan'),
        name: 'fightOrder',
        meta: { title: '拼单页面', icon: 'shopping', roles: ['store'] }
      }
    ]
  },
  {
    path: '/storeNotice',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'storeNotice',
    meta: {
      title: '通知',
      icon: 'table',
      roles: ['store']
    },
    children: [
      {
        path: 'getNotice',
        component: () => import('@/views/platform/notice/getNotice'),
        name: 'noticeMain',
        meta: { title: '通知页', icon: 'message', roles: ['store'] }
      }
    ]
  },
  {
    path: '/friend',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'Friend',
    meta: {
      title: '好友',
      icon: 'people',
      roles: ['customer']
    },
    children: [
      {
        path: 'myfriend',
        component: () => import('@/views/platform-user/friend/friend'),
        name: 'myfriend',
        meta: { title: '我的好友', icon: 'peoples', roles: ['customer'] }
      },
      {
        path: 'friendapply',
        component: () => import('@/views/platform-user/friend/friend-apply'),
        name: 'friendapply',
        meta: { title: '好友申请', icon: 'user', roles: ['customer'] }
      }
    ]
  },
  {
    path: '/pindan',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'Pindan',
    meta: {
      title: '拼单',
      icon: 'education',
      roles: ['customer']
    },
    children: [
      {
        path: 'pindanlist',
        component: () => import('@/views/platform-user/pindan/list-pindan'),
        name: 'pindanlist',
        meta: { title: '好友正在拼', icon: 'theme', roles: ['customer'] }
      },
      {
        path: 'mypindan',
        component: () => import('@/views/platform-user/pindan/my-launch-pindan'),
        name: 'mypindan',
        meta: { title: '我发起的拼单', icon: 'shopping', roles: ['customer'] }
      },
      {
        path: 'participatepindan',
        component: () => import('@/views/platform-user/pindan/my-participate-pindan'),
        name: 'participatepindan',
        meta: { title: '我参与的拼单', icon: 'international', roles: ['customer'] }
      }
    ]
  },
  {
    path: '/userNotice',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'userNotice',
    meta: {
      title: '通知',
      icon: 'table',
      roles: ['customer']
    },
    children: [
      {
        path: 'manageNotice',
        component: () => import('@/views/platform-user/notice/manageNotice'),
        name: 'noticeMain',
        meta: { title: '通知页', icon: 'message', roles: ['customer'] }
      }
    ]
  },
  {
    path: '/value',
    component: Layout,
    redirect: '/table/complex-table',
    name: 'userValue',
    meta: {
      title: '价值',
      icon: 'money',
      roles: ['customer']
    },
    children: [
      {
        path: 'valueMainPage',
        component: () => import('@/views/platform-user/value/valueMainPage'),
        name: 'valueMain',
        meta: { title: '欢迎页面', icon: 'chart', roles: ['customer'] }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // // mode: 'history', // require service support
  // scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
